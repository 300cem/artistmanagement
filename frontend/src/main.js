// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import App from './App'
import router from './router'
import './plugins/bootstrap-vue'
import firebase from 'firebase'

Vue.config.productionTip = false

firebase.initializeApp({
  apiKey: "AIzaSyB7YXxaxe18GTUpANZbP_cjs0kt5n6zczk",
    authDomain: "animeplacenavi.firebaseapp.com",
    projectId: "animeplacenavi",
    storageBucket: "animeplacenavi.appspot.com",
    messagingSenderId: "611558532528",
    appId: "1:611558532528:web:1b534c560e2a78a828a275"
})


/* eslint-disable no-new */
new Vue({
  router,
  render: h => h(App),
  created() {
    // Prevent blank screen in Electron builds
    
  }
}).$mount('#app')