import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
// TODO: import vue files from different page by a group Eg: Book / Slide
import PostDashboard from '@/components/Post/PostDashboard'
import BookDashboard from '@/components/Book/BookDashboard'
import SlideDashboard from '@/components/Slide/SlideDashboard'
import ArtistDashboard from '@/components/Artist/ArtistDashboard'
import DashboardList from '@/components/DashboardList'
import ContactUs from '@/components/ContactUs'
import EditPost from '@/components/Post/EditPost'
import EditBook from '@/components/Book/EditBook'
import EditSlide from '@/components/Slide/EditSlide'
import EditArtist from '@/components/Artist/EditArtist'
import CreatePost from '@/components/Post/CreatePost'
import CreateBook from '@/components/Book/CreateBook'
import CreateSlide from '@/components/Slide/CreateSlide'
import CreateArtist from '@/components/Artist/CreateArtist'
import PostDetail from '@/components/Post/PostDetail'
import BookDetail from '@/components/Book/BookDetail'
import ArtistDetail from '@/components/Artist/ArtistDetail'
import AllPostList from '@/components/Post/AllPostList'
import AllBookList from '@/components/Book/AllBookList'
import AllArtistList from '@/components/Artist/AllArtistList'
import Login from '@/components/User/Login'
import Register from '@/components/User/Register'
import Paginate from 'vuejs-paginate'
import VuePaginate from 'vue-paginate';
import lodash from 'lodash'
import vSelect from "vue-select";
// cookies
import VueCookies from 'vue-cookies'

Vue.use(Router)
Vue.use(lodash)
Vue.component('paginate', Paginate)
Vue.use(VuePaginate);
Vue.component("v-select", vSelect);
Vue.use(VueCookies)
Vue.$cookies.config('1d')

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/postDashboard',
      name: 'PostDashboard',
      component: PostDashboard
    },
    {
      path: '/bookDashboard',
      name: 'BookDashboard',
      component: BookDashboard
    },
    {
      path: '/slideDashboard',
      name: 'SlideDashboard',
      component: SlideDashboard
    },
    {
      path: '/artistDashboard',
      name: 'ArtistDashboard',
      component: ArtistDashboard
    },
    {
      path: '/dashboardList',
      name: 'DashboardList',
      component: DashboardList
    },
    {
      path: '/contactus',
      name: 'ContactUs',
      component: ContactUs
    },
    {
      path: '/editPost/:id',
      name: 'EditPost',
      component: EditPost
    },
    {
      path: '/editBook/:id',
      name: 'EditBook',
      component: EditBook
    },
    {
      path: '/editSlide/:id',
      name: 'EditSlide',
      component: EditSlide
    },
    {
      path: '/editArtist/:id',
      name: 'EditArtist',
      component: EditArtist
    },
    {
      path: '/createPost',
      name: 'CreatePost',
      component: CreatePost
    },
    {
      path: '/createBook',
      name: 'CreateBook',
      component: CreateBook
    },
    {
      path: '/createSlide',
      name: 'CreateSlide',
      component: CreateSlide
    },
    {
      path: '/createArtist',
      name: 'CreateArtist',
      component: CreateArtist
    },
    {
      path: '/postDetail/:id',
      name: 'PostDetail',
      component: PostDetail,
    },
    {
      path: '/bookDetail/:id',
      name: 'BookDetail',
      component: BookDetail
    },
    {
      path: '/artistDetail/:id',
      name: 'ArtistDetail',
      component: ArtistDetail,
    },
    {
      path: '/allPostList',
      name: 'AllPostList',
      component: AllPostList
    },
    {
      path: '/allBookList',
      name: 'AllBookList',
      component: AllBookList
    },
    {
      path: '/allArtistList',
      name: 'AllArtistList',
      component: AllArtistList
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
  ]
})
