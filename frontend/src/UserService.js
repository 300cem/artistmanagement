import axios from 'axios';
import Vue from 'vue'
import router from './router'

const loginUrl = 'http://localhost:3000/auth/'
const registerUrl = 'http://localhost:3000/users'

class UserService {
  
  // login 
  static login(email, password) {
    return axios.post(loginUrl, {
        email, password
    })
    .then((response) => {
      console.log("Message: " + response.data);
      alert("Login Successful")
      Vue.$cookies.set('userType', response.data.userType)
      Vue.$cookies.set('userName', response.data.name)
      console.log("userType", Vue.$cookies.get('userType'))
      router.push('/')
    })
    .catch(function (error) {
      if (error.response) {
        alert("Login Error: " + error.response.data.msg)
      }
    })
  }
  // register
  static register(name, email, password) {
    return axios.post(registerUrl, {
      name, email, password
    })
    .then((response) => {
      console.log("Message: " + response.data)
      alert(response.data)
      router.push('/login')
    })
    .catch(function (error) {
      if (error.response) {
        alert("Register Error: " + error.response.data.msg)
      }
    })
  }
}

export default UserService