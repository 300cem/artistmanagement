import axios from 'axios';

const url = 'http://localhost:3000/artists/'

class ArtistService {
  // get all artists
  static getArtists() {
    return new Promise((resolve, reject) => {
      axios.get(url).then((res) => {
        const data = res.data
        resolve(
          data.map(artist => ({
            ...artist,
            date: new Date(artist.date)
          }))
        )
      })
      .catch((err) => {
        reject(err)
      })
    })
  }

  static getArtistsById(id) {
    return new Promise((resolve, reject) => {
      axios.get(`${url}${id}`).then((res) => {
        const data = res.data
        resolve({data})
      })
      .catch((err) => {
        reject(err)
      })
    })
  }

  // create an artist data
  static insertArtist(name, image, dob, hobby, album, anime) {
    return axios.post(url, {
      name, image, dob, hobby, album, anime
    })
  }
  // update an artist data
  static updateArtist(id,name, image, dob, hobby, album, anime) {
    return axios.put(`${url}${id}`,{name, image, dob, hobby, album, anime})
  }
  // delete an artist data
  static deleteArtist(id) {
    return axios.delete(`${url}${id}`)
  }
}

export default ArtistService