import axios from 'axios';

const url = 'http://localhost:3000/slides/'

class SlideService {
  // get slide
  static getSlides() {
    return new Promise((resolve, reject) => {
      axios.get(url).then((res) => {
        const data = res.data
        resolve(
          data.map(slide => ({
            ...slide,
            date: new Date(slide.date)
          }))
        )
      })
      .catch((err) => {
        reject(err)
      })
    })
  }

  static getSlideById(id) {
    return new Promise((resolve, reject) => {
      axios.get(`${url}${id}`).then((res) => {
        const data = res.data
        resolve({data})
      })
      .catch((err) => {
        reject(err)
      })
    })
  }

  // create slide
  static insertSlide(text, image, imageUrl) {
    return axios.post(url, {
      text, image, imageUrl
    })
  }
  static updateSlide(id,text, image, imageUrl) {
    return axios.put(`${url}${id}`,{text, image, imageUrl})
  }

  // delete slide
  static deleteSlide(id) {
    return axios.delete(`${url}${id}`)
  }
}

export default SlideService