import axios from 'axios';

const url = 'http://localhost:3000/books/'

class BookService {
  // get book
  static getBooks() {
    return new Promise((resolve, reject) => {
      axios.get(url).then((res) => {
        const data = res.data
        resolve(
          data.map(book => ({
            ...book,
            date: new Date(book.date)
          }))
        )
      })
      .catch((err) => {
        reject(err)
      })
    })
  }

  // get one of book's data
  static getBooksById(id) {
    return new Promise((resolve, reject) => {
      axios.get(`${url}${id}`).then((res) => {
        const data = res.data
        resolve({data})
      })
      .catch((err) => {
        reject(err)
      })
    })
  }

  // create a book data
  static insertBook(title, artist, price, image) {
    return axios.post(url, {
      title, artist, price, image
    })
  }
  static updateBook(id, title, artist, price, image) {
    return axios.put(`${url}${id}`,{title, artist, price, image})
  }

  // delete a book data
  static deleteBook(id) {
    return axios.delete(`${url}${id}`)
  }
}

export default BookService