import axios from 'axios';

const url = 'http://localhost:3000/posts/'

class PostService {
  // get post
  static getPosts() {
    return new Promise((resolve, reject) => {
      axios.get(url).then((res) => {
        const data = res.data
        resolve(
          data.map(post => ({
            ...post,
            date: new Date(post.date)
          }))
        )
      })
      .catch((err) => {
        reject(err)
      })
    })
  }

  // get one of post's data
  static getPostById(id) {
    return new Promise((resolve, reject) => {
      axios.get(`${url}${id}`).then((res) => {
        const data = res.data
        resolve({data})
      })
      .catch((err) => {
        reject(err)
      })
    })
  }

  // create post
  static insertPost(title, content, artist, image) {
    return axios.post(url, {
      title, content, artist, image
    })
  }
  static updatePost(id, title, content, artist,image) {
    return axios.put(`${url}${id}`,{title, content, artist, image})
  }

  // delete post
  static deletePost(id) {
    return axios.delete(`${url}${id}`)
  }
}

export default PostService