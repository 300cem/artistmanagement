const express = require('express');
const router = express.Router();
const jwt = require ('jsonwebtoken');
const {check, validationResult} = require('express-validator/check');
const bcrypt = require('bcryptjs');
const UserModel =  require('../models/UserModel.js');

router.post('/',  [
    check('name', 'name is required').not().isEmpty(),
    check('email', 'Please enter a valid Email').isEmail(),
    check('password', 'Please Enter Password with at least six or more characters').isLength({
        min: 6})

],  async (req, res) => {
   const errors = validationResult(req);
   if( !errors.isEmpty){
       return res.status(400).json({errors: errors.array()})
   }  
  const {name, email, password, userType} = req.body;
  try {
      // if the user is already exists
      let user = await UserModel.findOne({email});
      if(user){
          return res.status(400).json({msg: 'User already Exists with Email Provided'});
      }
     
      user = new UserModel({
          name, 
          email,
          password,
          userType
      })
      // encrypt the password
      const salt = await bcrypt.genSalt(10);
      user.password = await bcrypt.hash(password, salt);
      await user.save();
      const payload = {
          user: {
              id: user.id
          }
      };
    } catch (error) {
    }
    res.send('Registration successful');
});
    
module.exports = router;