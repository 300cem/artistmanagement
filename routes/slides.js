const express = require('express');
const router = express.Router();
const Slide = require('../models/Slides');
const multer = require('multer');
const storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, './uploads');
    },
    filename: function(req, file, callback) {
        callback(null, new Date().toISOString() + file.originalname);
    }
})

const fileFilter = (req, file, callback) => {
    // filter file type
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        callback(null, true)
    } else {
        callback(null, false)
    }
}
const upload = multer({storage: storage, fileFilter: fileFilter})

// get Slide
router.get('/', async (req, res) => {
    try{
         const slides = await Slide.find();
         res.json(slides);
    }catch(err){
        res.json({message: err});
    }
});

// create a slide
router.post('/', async (req,res) => {
    console.log(req.file);
    const slide = new Slide({
        text: req.body.text,
        image: req.body.image,
        imageUrl: req.body.imageUrl
    });
    try{   
    const savedSlide = await slide.save()
        res.json(savedSlide)
    }catch (err) {
       res.json({message: err}); 
    }
});

// find a slide in slides
router.get('/:slideId', async (req,res) => {
    try {
         const slide = await Slide.findById(req.params.slideId);
         res.json(slide);
    }catch (err) {
        res.json({message: err});
    }    
});


// delete a slide
router.delete('/:slideId', async (req,res) => {
    try {
    const removedSlide = await Slide.remove({_id: req.params.slideId}) 
    res.json(removedSlide);
    }catch (err) {
    res.json({message: err});
    }
});


// update a slide
router.put('/:slideId', async (req,res) => {
    try{
    const updatedSlide = await Slide.updateOne(
        {_id: req.params.slideId}, 
        {$set: {text: req.body.text, 
                image: req.body.image,
                imageUrl: req.body.imageUrl
            }}
     );
     res.json(updatedSlide);    
    }catch (err) {
    res.json({message: err});
    }
})

module.exports = router;