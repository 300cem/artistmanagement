const express = require('express')
const { db } = require('../models/Navi')
const router = express.Router()
const Navi = require('../models/Navi')

router.get('/', async (req, res) => {
    try{
         const navis = await Navi.find();
         res.json(navis);
    }catch(err){
        res.json({message: err});
    }
});

router.get('/title', async(req, res) => {
        console.log(req.query.title);
        var query = {};
        if (req.query.title) query.title = req.query.title;
        await Navi.find(query, function(err, title){
        if(err) {
            res.send(err)
        } else {
            res.json(title)
            }
        })
    })

router.post('/', async (req, res) => {
    const navi = new Navi({
        title: req.body.title,
        locationName: req.body.locationName,
        lat: req.body.lat,
        lng: req.body.lng,
        image: req.body.image
    })
    try {
        const savedNavi = await navi.save()
            res.json(savedNavi)
    } catch (err) {
        res.json({message: err})
    }
})

router.delete('/:naviId', async(req, res) => {
    try {
        const removedNavi = await Navi.remove({_id: req.params.naviId})
        res.json(removedNavi)
    } catch {
        res.json({message: err})
    }
})

router.patch('/:naviId', async(req,res)=> {
    try {
        const uploadNavi = await Navi.updateOne(
            {_id: req.params.naviId},
            {$set: {title: req.body.title,
                    locationName: req.body.locationName,
                    lat: req.body.lat,
                    lng: req.body.lng,
                    image: req.body.image}}
        )
        res.json(uploadNavi)
    } catch (error) {
        res.json({message: err})
    }
})

module.exports = router