const express = require('express');
const router = express.Router();
const Post = require('../models/Posts');
const multer = require('multer');
const storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, './uploads');
    },
    filename: function(req, file, callback) {
        callback(null, new Date().toISOString() + file.originalname);
    }
})

const fileFilter = (req, file, callback) => {
    // filter file type
    if (file.mimetype === 'image/jpeg' || file.mimetyp === 'image/png') {
        callback(null, true)
    } else {
        callback(null, false)
    }
}
const upload = multer({storage: storage, fileFilter: fileFilter})

// get all posts
router.get('/', async (req, res) => {
    try{
         const posts = await Post.find();
         res.json(posts);
    }catch(err){
        res.json({message: err});
    }
});

// create a post
router.post('/', async (req,res) => {
    console.log(req.file);
    const post = new Post({
        title: req.body.title,
        content: req.body.content,
        artist: req.body.artist,
        image: req.body.image
    });
    try{   
    const savedPost = await post.save()
        res.json(savedPost)
    }catch (err) {
       res.json({message: err}); 
    }
});


// find a post by postId
router.get('/:postId', async (req,res) => {
    try {
         const post = await Post.findById(req.params.postId);
         res.json(post);
    }catch (err) {
        res.json({message: err});
    }    
});


// delete post
router.delete('/:postId', async (req,res) => {
    try {
    const removedPost = await Post.remove({_id: req.params.postId}) 
    res.json(removedPost);
    }catch (err) {
    res.json({message: err});
    }
});

//Update a post
router.put('/:postId', async (req,res) => {
    try{
    const updatedPost = await Post.updateOne(
        {_id: req.params.postId}, 
        {$set: {title: req.body.title,
                content: req.body.content,
                artist: req.body.artist, 
                image: req.body.image}}
     );
     res.json(updatedPost);    
    }catch (err) {
    res.json({message: err});
    }
})

module.exports = router;