const express = require('express');
const router = express.Router();
const Book = require('../models/Books');
const multer = require('multer');
const storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, './uploads');
    },
    filename: function(req, file, callback) {
        callback(null, new Date().toISOString() + file.originalname);
    }
})

const fileFilter = (req, file, callback) => {
    // filter file type
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        callback(null, true)
    } else {
        callback(null, false)
    }
}
const upload = multer({storage: storage, fileFilter: fileFilter})

// get book's data from API
router.get('/', async (req, res) => {
    try{
         const books = await Book.find();
         res.json(books);
    }catch(err){
        res.json({message: err});
    }
});

// create a new book data
router.post('/', async (req,res) => {
    console.log(req.file);
    const book = new Book({
        title: req.body.title,
        artist: req.body.artist,
        price: req.body.price,
        image: req.body.image
    });
    try{   
    const savedBook = await book.save()
        res.json(savedBook)
    }catch (err) {
       res.json({message: err}); 
    }
});

// find a book by bookId
router.get('/:bookId', async (req,res) => {
    try {
         const book = await Book.findById(req.params.bookId);
         res.json(book);
    }catch (err) {
        res.json({message: err});
    }    
});

// delete Book
router.delete('/:bookId', async (req,res) => {
    try {
    const removedBook = await Book.remove({_id: req.params.bookId}) 
    res.json(removedBook);
    }catch (err) {
    res.json({message: err});
    }
});


// update a book data
router.put('/:bookId', async (req,res) => {
    try{
    const updatedBook = await Book.updateOne(
        {_id: req.params.bookId}, 
        {$set: {title: req.body.title,
                artist: req.body.artist,
                price: req.body.price, 
                image: req.body.image}}
     );
     res.json(updatedBook);    
    }catch (err) {
    res.json({message: err});
    }
})


module.exports = router;