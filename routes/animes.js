const express = require('express')
const router = express.Router()
const Anime = require('../models/Animes')

router.get('/', async(req, res) => {
    try {
        const anime = await Anime.find()
        res.json(anime)
    } catch(err) {
        res.json({message: err})
    }
})

router.post('/', async (req, res) => {
    const anime = new Anime({
        title: req.body.title,
        year: req.body.year,
        image: req.body.image
    })
    try {
        const savedAnime = await anime.save()
            res.json(savedAnime)
    } catch (err) {
        res.json({message: err})
    }
})

router.get('/:animeId', async (req, res) => {
    try {
        const anime = await Anime.findById(req.params.animeId)
        res.json(anime)
    } catch {
        res.json({message: err})
    }
})

router.delete('/:animeId', async(req, res) => {
    try {
        const removedAnime = await Anime.remove({_id: req.params.animeId})
        res.json(removedAnime)
    } catch {
        res.json({message: err})
    }
})

router.patch('/:animeId', async(req,res)=> {
    try {
        const uploadAnime = await Anime.updateOne(
            {_id: req.params.animeId},
            {$set: {title: req.body.title,
                    year: req.body.year,
                    image: req.body.image}}
        )
        res.json(uploadAnime)
    } catch (error) {
        res.json({message: err})
    }
})

module.exports = router