const mongoose = require('mongoose');

const PostSchema = mongoose.Schema({
    title: { 
        type: String,
        require: true

    },
    content: {
        type: String,
        require: true
    },
    artist: {
        type: String,
        require: true
    },  
    image: {
        type: String,
        required: false
    },
    date: { 
        type: Date,
        default: Date.now
    }    
});

module.exports = mongoose.model('Posts', PostSchema);