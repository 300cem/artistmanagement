const mongoose = require('mongoose')

const AnimeSchema = mongoose.Schema({
    title: {
        type: String,
        require: true
    },
    year: {
        type: String,
        require: true
    },
    image: {
        type: String,
        require: false
    },
    updateDate: {
        type: Date,
        default: Date.now
    }
},{versionKey: false})

var mySchema = new mongoose.Schema({
    username:  'string'
},)

module.exports = mongoose.model('Animes', AnimeSchema)