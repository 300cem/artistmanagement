const mongoose = require('mongoose');

const ArtistSchema = mongoose.Schema({
    name: { 
        type: String,
        require: true

    },  
    image: {
        type: String,
        required: false
    },
    dob: {
        type: String,
        require: false,
        default: null
    },
    hobby: {
        type: String,
        require: true
    },  
    album: {
        type: String,
        require:true
    },
    anime: {
        type: String,
        require:true
    }, 
    date: { 
        type: Date,
        default: Date.now
    }    
});

module.exports = mongoose.model('Artists', ArtistSchema);