const mongoose = require('mongoose');

const SlideSchema = mongoose.Schema({
    text: { 
        type: String,
        require: true
    },  
    image: {
        type: String,
        required: false
    },
    imageUrl: {
        type: String,
        required: true
    },
    date: { 
        type: Date,
        default: Date.now
    }    
});

module.exports = mongoose.model('Slide', SlideSchema);