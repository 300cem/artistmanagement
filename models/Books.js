const mongoose = require('mongoose');

const BookSchema = mongoose.Schema({
    title: { 
        type: String,
        require: true

    },
    artist: {
        type: String,
        require:true
    },
    price: {
        type: String,
        require:true
    }, 
    image: {
        type: String,
        required: false
    },
    date: { 
        type: Date,
        default: Date.now
    }    

});


module.exports = mongoose.model('Books', BookSchema);