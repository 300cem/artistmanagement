const mongoose = require('mongoose')

const NaviSchema = mongoose.Schema({
    title: {
        type: String,
        require: true
    },
    locationName: {
        type: String,
        require: true
    },
    lat: {
        type: String,
        require: true
    },
    lng: {
        type: String,
        require: true
    },
    image: {
        type: String,
        require: false
    },
    updateDate: {
        type: Date,
        default: Date.now
    }
},{versionKey: false})

module.exports = mongoose.model('Navi', NaviSchema)